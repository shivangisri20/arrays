const nestedArray = [1, [2], [[3]], [[[4]]]];                       // array to test flat function

function flatten(elements) {
    let flat_array = [];
    for(let index  = 0 ;  index  < elements.length; index++) {
        if(Array.isArray(elements[index])) {
                
            flat_array = flat_array.concat(flatten (elements[index]));     // recursive calling 
                                      
        } else {

            flat_array.push(elements[index]);

             }
    }

   return flat_array;
}


let resultant_array = flatten(nestedArray);
console.log(resultant_array)