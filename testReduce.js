const items = [1, 2, 3, 4, 5, 5];                           // array to test reduce function

const cb = require ('./reduce.js');

               
// function each to perform .reduce()
function reduce(elements, cb, startingValue) {
    let result =0;

    if(startingValue){

        for(let i=0 ; i < elements.length; i++){

            startingValue = cb(startingValue ,  elements [i]);

        }
    }
    else {
         
        startingValue=elements[0];
        for(let i =1 ; i< elements.length ; i++){
            startingValue = cb(startingValue ,  elements [i]);
        }
    }
    result = startingValue ;
    console.log(result)
    
 }



reduce(items,cb,5); 
reduce(items,cb,); 
