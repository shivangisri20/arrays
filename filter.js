// Function cb  to perform find operation on element of the array 

function cb(array){

    let filtered_array = [] ;
    
    for(let i = 0 ; i < array.length ; i++){
        if(array[i] > 3 ){
            filtered_array.push(array[i]) ;
        }

     }

     return filtered_array ;
  
  }
  
  module.exports = cb;