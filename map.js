 // Function cb  to perform map operation on element of the array 

  function cb(array){

   for(let index=0 ; index < array.length ; index++){

    array[index] *= 2;
   }
   return array;

}

module.exports = cb;